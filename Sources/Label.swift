//
//  Label.swift
//  frontless
//
//  Created by Marcin Kliks on 01.07.2017.
//

import Foundation
import LeafProvider
import Leaf

fileprivate let templateContents = """
<p class="{{cssClass}}" style="{{inlineCssStyle}}" id="{{inputId}}">{{{contents}}}</p>
"""

public class Label: Widget {
    
    var contents: String
    
    init(_ pageView: PageView, inputId: String, contents: String) {
        self.contents = contents
        super.init(parent: pageView, inputId: inputId, template: templateContents)
    }
    
    public override var context: Node {
        return try! Node(node: [
            "contents" : Node(contents),
            "inputId" : Node(self.inputId),
            ])
    }
}

