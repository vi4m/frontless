//
//  Input.swift
//  frontless
//
//  Created by Marcin Kliks on 01.07.2017.
//

import Foundation
import Leaf
import LeafProvider

fileprivate let templateContents = """
<input class="form-control autocomplete="off" type="text" id="{{inputId}}" value="{{value}}" onchange="sendMessage('{{inputId}}', 'change', this.value)"></input>
"""

public class Input: Widget {
    
    var value: String
    
    init(_ pageView: PageView, inputId: String, value: String) {
        self.value = value
        super.init(parent: pageView, inputId: inputId, template: templateContents)
    }
    
    public override var context: Node {
        return try! Node(node: [
            "value" : Node(node: value),
            "inputId" : Node(node: self.inputId),
            ])
    }
    
    override public func getEvent(message: Message) {
        if message.command == .change {
            if let data = message.data {
                self.value = data
            }
            self.refresh()
        }
    }
}
