//
//  BaseWidgets.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 29.05.2016.
//
//

import LeafProvider
import Leaf
import Vapor
import Foundation

fileprivate let template = ""

open class Widget: Parentable {
    public weak var parent: Parentable?
    
    open var template: String
    open var inputId: String
    var style: String = ""
    
    open var context: Node {
        return ["test": "test"]
    }
    
    public func refresh() {
        (self.parent as! PageView).refresh(widget: self)
    }
    
    public func setStyle(style: String) {
        self.style = style
    }
    
    public func render() throws -> String {
        let context = Context((self.parent as! PageView).context)
        context.push(self.context)
        
        let leaf = try! stem.spawnLeaf(raw: template)
        let output = try! stem.render(leaf, with: context)
        return String(bytes: output, encoding: .utf8)!
    }
    
    public init(parent: Parentable?, inputId: String, template: String) {
                self.parent = parent
                self.inputId = inputId
                self.template = template
            }
    
    open func getEvent(message: Message) {
                preconditionFailure("Bla bla bla")
    }
}


