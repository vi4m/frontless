//
//  Globals.swift
//  htmlmakerTests
//
//  Created by Marcin Kliks on 29.06.2017.
//

import Foundation
import LeafProvider
import Leaf

internal let stem = Stem(DataFile(workDir: "/tmp"), cache: SystemCache<Leaf>(maxSize: 3.megabytes))

