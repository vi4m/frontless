//
//  Tools.swift
//  cal
//
//  Created by Marcin Kliks on 27.06.2017.
//
//

import Foundation

import LeafProvider
import Leaf
import Vapor
import Foundation



extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}


public func plugfrontless(droplet drop: Droplet) {
    
    drop.socket("/ws") { (request, ws) in
        
        background {
            while ws.state == .open {
                try? ws.ping()
                drop.console.wait(seconds: 10) // every 10 seconds
            }
        }
        ws.onText = { ws, text in
            print("Text received: \(text)")
            
            // FIXME: who will handle this?
            let message = try Message(input: text, socket: ws)
            frontlessDispatchMessage(message: message)
        }
        
        ws.onClose = { ws, code, reason, clean in
            print("Closed.")
        }
        
    }
    
}


