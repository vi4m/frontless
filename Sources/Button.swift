//
//  Button.swift
//  App
//
//  Created by Marcin Kliks on 24.06.2017.
//

import Foundation
import Leaf
import LeafProvider

public typealias optionalCallback<T> = ((_ self: T) -> Void)?


fileprivate let templateContents = """
<button type="button" class="btn btn-primary" style="#(style)" id="#(inputId)" onClick='sendMessage("click", "#(inputId)", "")'>#(value)</button>
"""


public class Button : Widget {
    public var caption: String
    var clickCallback: optionalCallback<Button>
    
    public init(_ parent: Parentable, inputId: String, caption: String,
                clickCallback: optionalCallback<Button>=nil) {
        self.caption = caption
        self.clickCallback = clickCallback
        super.init(parent: parent, inputId: inputId, template: templateContents)
    }
    
    public override var context: Node {
        return try! Node(node: [
            "value" : caption,
            "inputId": self.inputId,
            "style": self.style
            ])
    }
    
    public override func getEvent(message: Message) {
        if message.command == .clickEvent {
            self.clickCallback?(self)
        }
    }
}
