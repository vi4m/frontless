//
//  DataTable.swift
//  cal
//
//  Created by Marcin Kliks on 26.06.2017.
//
//

import Foundation
import LeafProvider
import Leaf
import Vapor
import Foundation

fileprivate let dataTableTemplate = """
<table class="table" id=#(inputId)>
#loop(rows, "row") {
#raw(row)
}
</table>
"""

public final class DataTableRow: Widget {
    var cells: [DataTableCell]
    
    public override var context: Node {
        let renderedCells = self.cells.map { (cell) in
            try! cell.render()
        }
        return try! Node(node: [
            "cells": renderedCells,
            "inputId": inputId
            ])
    }
    
    init(_ pageView: Parentable, inputId: String, _ cells: [DataTableCell]) {
        self.cells = cells
        super.init(parent: pageView, inputId: inputId, template: "")
    }
    
    init(_ pageView: Parentable, inputId: String, cells: [String]) {
        self.cells = cells.map { el in
            DataTableCell(pageView, inputId: inputId, content: el)
        }
        super.init(parent: pageView, inputId: inputId, template: """
<tr id="#(inputid)">#loop(cells, "cell"){ #raw(cell) }</tr>
""")
    }
}

public final class DataTableCell: Widget {
    
    public var content: String
    
    init(_ pageView: Parentable, inputId: String, content: String) {
        self.content = content
        super.init(parent: pageView, inputId: inputId, template: """
<td id="#(inputid)">#(content)</td>
""")
    }
    public override var context: Node {
        return try! Node(node: ["content": self.content])
    }
}


public final class DataTable:  Widget {
    public var rows: [DataTableRow]
    
    public override var context: Node {
        
        let renderedRows = self.rows.map { (row) in
            try! row.render()
        }
        return try! Node(node: [
                "inputId": inputId,
                "rows": renderedRows
            ])
    }
    
    public init(_ pageView: Parentable, inputId: String = "", rows: [DataTableRow]) {
        self.rows = rows
        super.init(parent: pageView, inputId: inputId, template: dataTableTemplate)
    }
    
    public init(_ pageView: Parentable, inputId: String = "", strings: [[String]]) {
        self.rows = strings.map { s  in
            DataTableRow(pageView, inputId: inputId, cells: s)
        }
        super.init(parent: pageView, inputId: inputId, template: dataTableTemplate)
    }
    
    public func addRow(_ row: DataTableRow) {
        self.rows.append(row)
    }
    
    public func addRow(_ row: [String]) {
        self.rows.append(DataTableRow(self, inputId: "", cells: row))
    }
}
