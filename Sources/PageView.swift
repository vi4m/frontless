//
//  PageView.swift
//  cal
//
//  Created by Marcin Kliks on 27.06.2017.
//
//

import Foundation
import LeafProvider
import Leaf
import Vapor
import Foundation


public protocol Parentable: class {
    weak var parent: Parentable? { get set }
}



open class PageView: Parentable  {
    
    public var parent: Parentable? = nil
    var widgets: [String: Widget]
    private(set) public var session: frontlessSession
    
    open var templateName: String  = "test"
    
    var context: Node {
        
        return try! Node(node: ["sessionId": self.session.sessionId])
        
    }
    
    required public init(session: frontlessSession) {
        defer {
            self.onViewCreated()
        }
        self.session = session
        self.widgets = [:]
    }
    
    deinit{
        print("pageview is being released")
    }
    
    open func onLoad() {
        // browser loaded the view - websocket connected
    }
    
    open func onClose() {
        
    }
    
    open func onViewCreated() {
        
    }
    
    public func showMessage(_ message: String) {
        try! self.sendMessage(Message(sessionId: self.session.sessionId, command: .showMessage, componentId: "", data: message))
    }
    
    
    public func addWidget(widget: Widget) {
        // FIXME: detect collisions
        self.widgets[widget.inputId] = widget
    }
    
    public func sendMessage(_ message: Message) throws {
        self.session.sendMessage(message: message)
    }
    
    public func getEvent(message: Message) {
        guard let widget = self.widgets[message.componentId] else {
            fatalError("Unknown widget")
        }
        
        widget.getEvent(message: message)
    }
    
    public func render() throws -> String {
        let view = LeafRenderer(viewsDir:
            "/Users/vi4m/Documents/work/hutacal/cal/Resources/Views/")
        
        let children = try! self.renderChildren()
        let context = Context(self.context)
        context.push(children)
        let res = try view.make("\(self.templateName).leaf", context)
        return String(bytes: res.makeBytes(), encoding: .utf8)!
    }
    
    public func renderChildren() throws -> Node {
        var ret = [String:String]()
        for (inputName, widget) in self.widgets {
            ret[inputName] = try widget.render()
        }
        print(ret)
        return try! Node(node: ret)
    }
    
    func refresh(widget: Widget) {
        guard let widget = self.getWidget(widget.inputId) else {
            abort()
        }
        let render = try! widget.render()
        do {
            try self.sendMessage(Message(
                sessionId: self.session.sessionId,
                command: .refreshComponent,
                componentId: widget.inputId,
                data: render)
            )
        }
        catch {
            print(error)
        }
    }
    
    func getWidget(_ widget: String) -> Widget? {
        return self.widgets[widget]
    }
    
}


