//
//  WebsocketDispatch.swift
//  Run
//
//  Created by Marcin Kliks on 18.06.2017.
//

import Foundation
import Vapor

let MESSAGE_DELIMITER = "###"

public var frontlessSessions = [String: PageView]()


enum MessageError: Error {
    case InvalidMessageFormat(String)
}



public struct Message {
    var sessionId: String
    public var command: MessageCommand
    var componentId: String
    var data: String?
    weak var socket: WebSocket!
    
    public func toString() -> String {
        let components: [String] = [sessionId, command.rawValue, componentId, data ?? ""]
        return components.joined(separator: MESSAGE_DELIMITER)
    }
    
    public init(sessionId: String, command: MessageCommand, componentId: String,
                data: String?) {
        self.sessionId = sessionId
        self.command = command
        self.componentId = componentId
        self.data = data
    }
    
    public init(input: String, socket: WebSocket) throws {
        self.socket = socket
        var splitted = input.components(separatedBy: MESSAGE_DELIMITER)
        
        guard splitted.count == 4 else {
            throw MessageError.InvalidMessageFormat(input)
        }

        guard let command = MessageCommand(rawValue: splitted[1]) else {
            fatalError("Unknown command \(splitted[1])")
        }
        sessionId = splitted[0]
        self.command = command
        componentId =  splitted[2]
        data = (splitted[3] == "") ? nil: splitted[3]
    }
}


public enum MessageCommand: String {
    public typealias RawValue = String
    
    case startSession = "start"
    case refreshComponent = "refresh"
    case endSession = "end"
    case clickEvent = "click"
    case showMessage = "showMessage"
    case change = "change"
}

public func getOrCreateSession(sessionId: String) -> frontlessSession {
    if let pageView = frontlessSessions[sessionId] {
        return pageView.session
    } else {
        return frontlessSession(sessionId: sessionId)
    }
}


public class frontlessSession {
    var clients: [WebSocket] = []
    private(set) public var sessionId: String
    
    public init(sessionId: String? = nil) {
        self.sessionId = sessionId ?? UUID().uuidString
    }
    
    public func sendMessage(message: Message) {
        print("Clients count: ", clients.count)
        for client in clients {
            try? client.send(message.toString())
        }
    }
}

public func frontlessDispatchMessage(message: Message) {
    guard let view = frontlessSessions[message.sessionId] else {
        print("Cannot find session: \(message.sessionId)")
        return
    }
    
    print("Sessions: \(frontlessSessions.count)")

    switch(message.command) {
    case .startSession:
        view.session.clients.append(message.socket)
        print("count", view.session.clients.count)
        view.onLoad()
    case .clickEvent:
        view.getEvent(message: message)
    case .refreshComponent:
        break
    case .endSession:
        view.onClose()
    case .showMessage:
        break
    case .change:
        view.getEvent(message: message)
    }
}



