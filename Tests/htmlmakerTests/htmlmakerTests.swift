import XCTest
@testable import frontless


class EmptyScope: PageView {
    
    
}

let scope = EmptyScope(session: frontlessSession(sessionId: "test"))


class htmlmakerTests: XCTestCase {
    func testWidget() {
        let widget = Widget(parent: scope, inputId: "test", template: "widget")
        let expectedHTML = "widget"
        XCTAssertEqual(try! widget.render(), expectedHTML)
        print(MemoryLayout.stride(ofValue: widget))
    }
    
    static var allTests = [
        ("testWidget", testWidget),
        ("testDataTable", testDataTable)
    ]
    
    func testDataTable() {
        let table = DataTable(scope, inputId: "test", strings: (1...2).map({ (item) -> [String] in
            ["col1"]
        }))
        var output: String = ""
        measure {
         output = try! table.render()
        }
        
        XCTAssertEqual(output,
        """
        <table class="table" id=test>
        <tr id=""><td id="">col1</td></tr>
        <tr id=""><td id="">col1</td></tr>
        </table>
        """)
    }
}
