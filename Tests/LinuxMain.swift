import XCTest
@testable import htmlmakerTests

XCTMain([
    testCase(htmlmakerTests.allTests),
])
