//
//  BaseWidgets.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 29.05.2016.
//
//





public class Widget: MustacheBoxable {
    var pageView: PageView
    var templateName: String
    var inputId: String
    
    public var mustacheBox: MustacheBox {
    
        return Box(boxable: "test" )
    }
    
    init(_ pageView: PageView, inputId: String, templateName: String) {
        self.pageView = pageView
        self.inputId = inputId
        self.templateName = templateName
        self.pageView.widgets[inputId] = self
    }
    
    deinit{
        print("widget \(self.templateName) is being released")
    }
    
    func getEvent(message: String, data: String?) {
        preconditionFailure("Bla bla bla")
    }
    
    func refresh() {
        self.pageView.refresh(widget: self)
    }
    
    func render() throws -> String {
        
        var template: Template?
        
        do {
            template = try repository.template(named: self.templateName)
//            template?.registerInBaseContext(key: "each", Box(StandardLibrary.each))
//            template?.registerInBaseContext(key: "each", MustacheBox(filter: StandardLibrary.each))
        }
        catch {
            throw MustacheError(kind: .TemplateNotFound, message: "\(templateName): \(String(error))")
        }
        
        let rendering = try! template!.render(box: Box(boxable: self))
        return rendering
    }
    
}



public class PageView: MustacheBoxable  {
  
    var widgets: [String: Widget]
    var sessionId: String
    var webSockets: [WebSocket] = []
    var messages = Channel<Message>()
    var templateName: String
    
    init(sessionId: String, templateName: String) {
        defer {
            self.onViewCreated()
        }
        self.sessionId = sessionId
        self.templateName = templateName
        self.widgets = [:]
    }
    
    deinit{
        print("pageview is being released")
    }
    
    
     public var mustacheBox: MustacheBox {
        var boxable = [String:String]()
        boxable["sessionId"] = sessionId
        
        do {
            for (widget_name, widget) in self.widgets {
                let output = try widget.render()
                boxable[widget_name] = output
            }
        } catch {
            return Box(boxable: ["error": String(error)])
        }
        return Box(boxable: boxable)
    }
    
    func onLoad() {
       // browser loaded the view - websocket connected
    }
    
    func onViewCreated() {
        
       // just initialized the pageview
    }
    
    func addWidget(widget: Widget) {
        self.widgets[widget.inputId] = widget
    }
    
    func send(_ message: String) throws {
        for socket in self.webSockets {
            messages.send(Message(socket: socket, message: message))
        }
    }
    
    func getEvent(id: String, message: String, data: String?) {
        self.widgets[id]?.getEvent(message: message, data: data)
    }
    
    func render() throws -> String {
        
        var template: Template?
        
        do {
            template = try repository.template(named: self.templateName)
//            template?.registerInBaseContext(key: "each", Box(StandardLibrary.each))
//            template?.registerInBaseContext(key: "each", MustacheBox(filter: StandardLibrary.each))
        }
        catch {
            throw MustacheError(kind: .TemplateNotFound, message: "\(templateName): \(String(error))")
        }
        
        let rendering = try! template!.render(box: Box(boxable: self))
        return rendering
    }
    
    func refresh(widget: Widget) {
        guard let widget = self.getWidget(widget.inputId) else {
            abort()
        }
        let render = try! widget.render()
        do {
            try self.send(self.sessionId + "###" + widget.inputId + "###" + render)
        }
        catch {
           print(error)
        }
    }
    
    func getWidget(_ widget: String) -> Widget? {
        return self.widgets[widget]
    }

}

    
