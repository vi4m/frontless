//
//  Session.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 29.05.2016.
//
//

import Foundation
import WebSocket

func Session () -> String {
        return NSUUID().uuidString
}

var sessions: [String:PageView] = [:]


struct Message {
    var socket: WebSocket
    var message: String
    
}


