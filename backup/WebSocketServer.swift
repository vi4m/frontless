//
//  WebSocketServer.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 27.05.2016.
//
//


import HTTPServer
import Mustache
import Sideburns
import WebSocketServer




let webSocketServer = WebSocketServer.Server { (webSocket, request) in

    var closed: Bool = false
    var currentSessionId: String? = nil
   
    print("CONNECTED");
    
    webSocket.onBinary { data in
        print("data \(data)")
    }
    
    webSocket.onClose { reason, code in
        guard let sessionsId = currentSessionId else {
            return
        }
        
        print("CLOSED--------------------------")
//        sessions[sessionsId]?.widgets = [:]
//        sessions[sessionsId] = nil
        
        
//        closed = true
//        for (session, pv) in sessions {
//            var list = sessions[session]!.webSockets
//            
//            var index = list.index { (socket) -> Bool in
//                return socket === webSocket
//            }
//            if let index = index {
//                list.remove(at: index)
//                print("Closed: \(index)")
//                sessions[session] = nil
//            }
//        }
    }
    
    webSocket.onText { text in
        print("Got: \(text)")
        var sessionId, componentId, message, data: String
        var splitted = text.split(byString: "###")
        sessionId = splitted[0]
        componentId = splitted[1]
        message = splitted[2]
        data = splitted[3]
        
        currentSessionId = sessionId
        guard let page = sessions[sessionId] else {
            return
        }
        
        if componentId == "main" {
            if page.webSockets.count == 0 {
                
                co {
                    while true {
                        
                        if let msg = page.messages.receive() {
                            
                            if currentSessionId == nil {
                                print("Session not started!")
                                return
                            }
                            print("Recieved: \(msg.socket)")
                            do {
                                if closed {
                                    print("Resign...")
                                    return
                                }
                                try msg.socket.send(msg.message)
                            }
                            catch {
                                print(error)
                            }
                        }
                    }
                }
                
                sessions[sessionId]?.webSockets.append(webSocket)
                sessions[sessionId]?.onLoad()
            }
        }
        sessions[sessionId]?.getEvent(id: componentId, message: message, data: data)
    }
    
}
