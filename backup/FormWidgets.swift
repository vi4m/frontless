//
//  DataTable.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 27.05.2016.
//
//

import Mustache
import Sideburns






public class SearchEditOptions: Widget {
    var allOptions: [Int:String] = [:]
    var options: [Int:String] = [:]
    var visible: Bool = false
    var selectedKey: Int?
    var searchEdit: SearchEdit?
  
    init(_ pageView: PageView, searchEdit: SearchEdit, inputId: String, selectedIndex: Int, options: [Int: String]) {
        super.init(pageView, inputId: inputId, templateName: "SearchEditOptions")
        self.searchEdit = searchEdit
        self.allOptions = options
    }
    
    func filterOptionsBy(_ string: String) {
        
        let filtered = self.allOptions.filter { $0.1.starts(with: string) }
        var newData = [Int:String]()
        for (key, value) in filtered {
            newData[key] = value
        }
        self.options = newData
    }
    
    
    public override var mustacheBox: MustacheBox {
        var sessionId = self.pageView.sessionId
        //else {
        //    print(#file + "unitialized widget: \(self.inputId)")
        //    abort()
            
        //}
        return Box(boxable: [
                                "inputId": Box(boxable: self.inputId),
                                "display": Box(boxable: visible ? "box": "none"),
                                "options": Box(boxable: options.map {(key, value) in
                                    ["key": Box(boxable: key), "value": Box(boxable: value)]
                                }
                                ),
                                "sessionId": Box(boxable: sessionId)
        ])
    }
    
    override func getEvent(message: String, data: String?) {
        if message == "selected" {
            guard let data = data else {
                print("Got event without data ?")
                return
            }
            self.selectedKey = Int(data)
            self.searchEdit?.getEvent(message: message, data: data)
        }
    }
    
}

public class SearchEdit: Widget {
    
    var value: String
    var options: SearchEditOptions?
  
    init(_ pageView: PageView, inputId: String, value: String, options: [String]) {
        self.value = value
        super.init(pageView, inputId: inputId, templateName: "SearchEdit")
        self.options = SearchEditOptions(
            pageView, searchEdit: self , inputId: inputId + "_options",
            selectedIndex: 0, options: [1: "raz", 2: "dwa", 3: "trzy"]
        )
        self.options?.filterOptionsBy(self.value)
    }
    
    
    public override var mustacheBox: MustacheBox {
        return Box(boxable: [
                                "value": Box(boxable: value),
                                "options": Box(boxable: try! self.options!.render()),
                                "inputId": Box(boxable: self.inputId),
                                "sessionId": Box(boxable: self.pageView.sessionId)
        ])
    }
    
    
    override func getEvent(message: String, data: String?) {
        if message == "change" {
            self.value = data!
            self.options?.filterOptionsBy(self.value)
            self.options?.refresh()
        }
        else if message == "focus" {
            self.options?.visible = true
            self.options?.refresh()
        }
        else if message == "blur" {
            nap(for: 0.2.seconds)
            self.options?.visible = false
            self.options?.refresh()
        }
    }
}
