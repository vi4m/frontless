//
//  FileListView.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 27.05.2016.
//
//

import HTTPServer
import Mustache
import Sideburns
import WebSocket


let repository = TemplateRepository(dataSource: ZewoDataSource())


private class OKButton: Button {
    
    override func getEvent(message: String, data: String?) {
        let flv = self.pageView as! FilesListView
        if message == "click" {
//            flv.loadDocsData()
        }
    }
}

private class AddButton: Button {
    override func getEvent(message: String, data: String?) {
        let flv = self.pageView as! FilesListView
        if message == "click" {
            try! flv.send("redirect: /add")
        }
    }
}


public class FilesListView: PageView {
    
    private var button : OKButton?
    private var addButton: AddButton?
    var status: Input?
    public var table: DataTable?
    
    
    init(sessionId: String) {
        super.init(sessionId: sessionId, templateName: "ListView")
   }
    
    override func onViewCreated() {
        
        button = OKButton(self, inputId:"button", caption: "Refresh")
        addButton = AddButton(self, inputId:"addButton", caption: "Add new")
        table = DataTable(self, inputId: "table",  rows: [[]])
        status = Input(self, inputId: "status", value: "Status tutaj wpisz")
//        self.loadDocsData()
    }
    
    
    func loadDocsData() {
        
       
        let gd = GDocs()
        let files = try! gd.getFilesList()
        
        for file in files["files"]!.array! {
            print(file)
            let type = file["kind"]!.string!
            let name = file["name"]!.string!
            let id = file["id"]!.string!
            let mime = file["mimeType"]!.string!
            let webViewLink = file["webViewLink"]!.string!
            let iconLink = file["iconLink"]!.string!
            let thumbnailLink = file["thumbnailLink"]?.string ?? ""
            let modifiedTime = file["modifiedTime"]!.string!
            let lastModifiedUser = file["lastModifyingUser"]?["displayName"]!.string!
            let lastModifiedUserImage = file["lastModifiedUser"]?["photoLink"]!.string!
            
            self.table?.addRow([name, "<a href='\(webViewLink)'>Open</a>","<img src='\(thumbnailLink)'></img>"])
        }
        self.table?.refresh()
    }
    
    
    override func onLoad() {
//        self.table?.rows = []
//        self.table?.refresh()
//        self.status?.value = "Loading..."
//        self.status?.refresh()
//        self.loadDocsData()
    }
}