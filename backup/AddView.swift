//
//  AddView.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 05.06.2016.
//
//

//
//  FileListView.swift
//  docs-proposals
//
//  Created by Marcin Kliks on 27.05.2016.
//
//

import HTTPServer
import Mustache
import Sideburns
import WebSocket

class MySearch: SearchEdit {
    
    override func getEvent(message: String, data: String?) {
        super.getEvent(message: message, data: data)
        if message == "selected" {
                try? self.pageView.send("redirect: /add")
            }
        }
    }


public class AddView: PageView {
    
    private var search: [SearchEdit] = []
    
     init(sessionId: String) {
        
        super.init(sessionId: sessionId, templateName: "AddView")
        
        templateName = "AddView"
       
//        for i in 0...100 {
//            
//            self.search.append(MySearch(
//                self,
//                inputId: "search\(i)",
//                value: "dupa",
//                options: ["raz", "dwa", "trzy", "cztery"]
//            ))
//        }
    }
    
    override func onLoad() {
    }
}

